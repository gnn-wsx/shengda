# 达尔文的课表（基于遗传算法自动排课系统）

#### 介绍
基于遗传算法自动排课系统
##业务背景：鉴于当前排课软件的局限，无法排除节假日以及学校大型活动，本项目应运而生。其目标是通过技术手段提高排课的灵活性和效率。
##核心功能：项目的核心功能是排课，而这一过程通过遗传算法的应用得以实现。利用遗传算法，我们能够有效解决时间、班级、教师之间的冲突。此外，借助Swagger文档编写，实现了清晰易懂的接口文档，提高了项目的可维护性。
##业务目标：通过调用节假日API获取本年度假期日历，项目旨在通过遗传算法对非节假日日期进行智能排课。该算法能够高效地解决排课中的复杂问题，并且通过在线节假日API实时获取详细假日信息，支持学校活动的灵活设置。


#### 软件架构
软件架构说明
项目技术栈：采用Vue+Element UI+SpringBoot+MySQL技术栈，结合现代前端框架和后端开发技术，以满足项目的需求。
##项目展示
![输入图片说明](https://foruda.gitee.com/images/1699862558978848951/3c92f663_12859460.png "屏幕截图 2023-11-13 144638.png")
![输入图片说明](https://foruda.gitee.com/images/1699862578863987764/247ecc9e_12859460.png "屏幕截图 2023-11-13 144721.png")
![输入图片说明](https://foruda.gitee.com/images/1699862590789960117/ec102e8a_12859460.png "屏幕截图 2023-11-13 145038.png")

#### 安装教程

1.  进入一个文件夹命令行或终端输入：git clone https://gitee.com/shen-shengda/shengda.git 
 如果想要克隆其他分支：git clone -m 分支名 https://gitee.com/shen-shengda/shengda.git
2.  将db文件中数据库文件运行
3.  修改 .yml 文件中数据库信息 
4. 运行项目，接口文档地址：http://localhost:8085/swagger-ui.html

#### 使用说明
1.  排课时要传入要排课学期，和开课时间，最好选周一，然后会进入遗传算法接口进行排课
2.  因为排课，编码基因初始量已经很大，不需要很多遗传代数，推荐5——12次遗传


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
