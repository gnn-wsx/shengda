package cn.shen.sheng.da.controller;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.entity.AdminEntity;
import cn.shen.sheng.da.model.vo.request.AdminLoginRequest;
import cn.shen.sheng.da.service.AdminService;
import cn.shen.sheng.da.service.impl.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/admin")
@Api(tags = "管理员接口")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private TokenService tokenService;
    /*
    * 排课管理员登录
    * */
    @ApiOperation(value = "管理员登录",notes = "学生type=1，教师type=2，管理员type=3")
    @PostMapping("/login")
    public ServerResponse adminLogin(@RequestBody AdminLoginRequest adminLoginRequest){
        System.out.println(adminLoginRequest);
        Map<String ,Object> map = new HashMap<>();
        AdminEntity admin = adminService.adminLogin(adminLoginRequest.getAdminUsername(), adminLoginRequest.getAdminPassword());
        if (admin != null){
            String token = tokenService.getToken(admin);
            map.put("admin", admin);
            map.put("token", token);
            return ServerResponse.ofSuccess(map);
        }
        return ServerResponse.ofError("用户名或密码错误!");
    }
        /**
         * 管理员更新个人资料
         * @return
         */
        @ApiOperation(value = "管理员更新个人资料",notes = "根据adminId更新")
        @PostMapping("/modify")
        public ServerResponse modifyAdmin(@RequestBody AdminEntity admin) {

            return adminService.updateById(admin) ? ServerResponse.ofSuccess("更新成功！") : ServerResponse.ofError("更新失败！");
        }
    /**
     * 根据ID查询管理员信息
     * @param id
     * @return
     */
    @ApiOperation(value = "查询管理员信息",notes = "根据adminId查询")
    @GetMapping("/{id}")
    public ServerResponse queryAdmin(@PathVariable("id") Integer id) {
        return ServerResponse.ofSuccess(adminService.getById(id));
    }



}
