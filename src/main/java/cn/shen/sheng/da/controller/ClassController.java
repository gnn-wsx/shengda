package cn.shen.sheng.da.controller;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.entity.ClassEntity;
import cn.shen.sheng.da.service.ClassService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 班级信息
 */
@Api(tags = "班级信息接口")
@RestController
@RequestMapping("/admin/class")
public class ClassController {

    @Autowired
    private ClassService classService;
    /*
    * 分页查询班级信息
    * */
    @ApiOperation(value = "分页查询班级信息，包含班级人数",notes = "默认第一页")
    @GetMapping("/page")
    public ServerResponse getPage() {
        return classService.classInfo(1);
    }
    @ApiOperation(value = "分页查询班级信息，含班级人数",notes = "输入要查询页数，index")
    @GetMapping("/page/{index}")
    public ServerResponse getPage(@PathVariable Integer index) {
        return classService.classInfo(index);
    }
    @ApiOperation(value = "分页查询班级信息，包含班级人数",notes = "默认第一页")
    @GetMapping("/page/listName")
    public ServerResponse getPageInfo() {
        return classService.listName(1);
    }
    @ApiOperation(value = "分页查询班级信息，不含班级人数",notes = "输入要查询页数")
    @GetMapping("/page/listName/index/{index}")
    public ServerResponse getPageInfo(@PathVariable Integer index) {
        return classService.listName(index);
    }
    @ApiOperation(value = "根据班级名称查询",notes = "默认第一页，className")
    @GetMapping("/page/keyword/{keyword}")
    public ServerResponse getPageByKeyword(@PathVariable String keyword){
        return (classService.classSelectLike(keyword,1)) ;
    }
    @ApiOperation(value = "根据班级名称查询",notes = "输入要查询页数index，班级名称className")
    @GetMapping("/page/keyword/index/{keyword}/{index}")
    public ServerResponse getPageByKeywordIndex(@PathVariable String keyword,@PathVariable Integer index){
        return classService.classSelectLike(keyword,index);
    }
    @ApiOperation(value = "添加新的班级",notes = "输入要添加班级名称className，教室room")
    @PostMapping("/addClassInfo")
    public ServerResponse addClass(@RequestBody ClassEntity classEntity) {
        ClassEntity c = new ClassEntity();
        c.setClassName(classEntity.getClassName());
        c.setRoom(classEntity.getRoom());
        boolean b = classService.save(c);
        if (b) {
            return ServerResponse.ofSuccess("添加班级成功");
        }
        return ServerResponse.ofError("添加班级失败");
    }
    @ApiOperation(value = "删除班级",notes = "根据classId删除")
    @PostMapping("/removeClassInfo/{classId}")
    public ServerResponse removeClass(@PathVariable Integer classId) {

        boolean b = classService.removeById(classId);
        if (b) {
            return ServerResponse.ofSuccess("删除班级成功");
        }
        return ServerResponse.ofError("删除班级失败");
    }
    @ApiOperation(value = "更新班级信息",notes = "只更新classes表内容")
    @PostMapping("/updateClassInfo")
    public ServerResponse updateClass(@RequestBody ClassEntity classEntity) {
        return classService.updateById(classEntity) ? ServerResponse.ofSuccess("更新成功") : ServerResponse.ofError("更新失败");
    }
    @ApiOperation(value = "统计班级分页下标页码")
    @GetMapping("/page/count")
    public ServerResponse pageCount(){
        return ServerResponse.ofSuccess(classService.count());
    }
    @ApiOperation(value = "查询所有班级",notes = "返回所有classID对象")
    @RequestMapping("/names")
    public ServerResponse listName() {
        QueryWrapper<ClassEntity> classEntityQueryWrapper = new QueryWrapper<ClassEntity>();
        classEntityQueryWrapper.select("class_id");
        List<ClassEntity> list = classService.list(classEntityQueryWrapper);
        List<Map<String,Integer>> classID = new ArrayList<>();
        for (ClassEntity classEntity : list){
            Map<String, Integer> map = new HashMap<>();
            map.put("classId", classEntity.getClassId());

            classID.add(map);
        }
        return ServerResponse.ofSuccess(classID);
    }




}
