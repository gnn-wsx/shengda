package cn.shen.sheng.da.controller;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.entity.CourseEntity;
import cn.shen.sheng.da.service.CourseService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
@Api(tags = "课程信息接口")
@RestController
@RequestMapping("/admin/course")

public class CourseController {
    @Autowired
    private CourseService service;
    @ApiOperation(value = "分页查询课程信息",notes = "默认第一页")
    @GetMapping("/page")
    public ServerResponse getPage(){
        return service.selectCourseByPage(1);
    }
    @ApiOperation(value = "分页查询班级信息，含班级人数",notes = "输入要查询页数，index")
    @GetMapping("/page/{index}")
    public ServerResponse getPage(@PathVariable Integer index){
        return service.selectCourseByPage(index);
    }
    @ApiOperation(value = "新增课程",notes = "课程号courseId：自动生成不用输，课程等级courseLevel：course_level," +
            "0-10的话教师表中course_level为1的老师能教，11-20的话教师表中course_level为2的老师能教，" +
            "21-30的话教师表中course_level为3的老师能教，教师表中course_level为0的老师能教所有课程，课程名：courseName，" +
            "课程排序：courseOrder，课程状态：courseStatus：0代表未排课，1已排课，2已结课" +
            "课时：hours，课程所在学期：term")
    @PostMapping
    public ServerResponse create(@RequestBody @Validated CourseEntity course){

            return ServerResponse.ofSuccess(service.save(course));

    }
    @ApiOperation(value = "根据courseId查询课程，方便更新操作")
    @GetMapping("/{courseId}")
    public ServerResponse get(@PathVariable Integer courseId) {
        return ServerResponse.ofSuccess(service.getById(courseId));
    }
    @ApiOperation(value = "删除课程",notes = "根据课程号：courseId删除")
    @DeleteMapping("/{courseId}")
    public ServerResponse deleteItem(@PathVariable Integer courseId) {
        return ServerResponse.ofSuccess(service.removeById(courseId));
    }
    @ApiOperation(value = "更新课程信息",notes = "根据课程号：courseId更新")
    @PutMapping
    public ServerResponse modifyCourseInfo(@RequestBody @Validated CourseEntity entity) {
        return ServerResponse.ofSuccess(service.update(entity));
    }
    @ApiOperation(value = "根据课程名称查询",notes = "输入要查询页数index，班级名称className")
    @GetMapping("/search/{page}/{keyword}")
    public ServerResponse searchCourseInfo(@PathVariable("page") Integer page,
                                           @RequestParam(defaultValue = "10") Integer limit,
                                           @PathVariable("keyword") String keyword) {
        QueryWrapper<CourseEntity> wrapper = new QueryWrapper<>();
        wrapper.like("course_name", keyword);
        Page<CourseEntity> pages = new Page<>(page, limit);
        IPage<CourseEntity> iPage = service.page(pages, wrapper);
        return ServerResponse.ofSuccess(iPage.getRecords());
    }
    @ApiOperation(value = "统计课程分页下标页码")
    @GetMapping("/page/count")
    public ServerResponse pageCount(){
        return ServerResponse.ofSuccess(service.count());
    }



}
