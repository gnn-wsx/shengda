package cn.shen.sheng.da.controller;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.entity.HolidayVo;
import cn.shen.sheng.da.service.HolidayVoService;
import cn.shen.sheng.da.util.HolidayUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Api(tags = "节假日信息接口")
@RestController
@RequestMapping("admin/holiday")
public class HolidayController {

    private final HolidayVoService holidayVoService;

    @Autowired
    public HolidayController(HolidayVoService holidayVoService) {
        this.holidayVoService = holidayVoService;
    }

    // 写入数据
    @ApiOperation(value = "将利用api查询到日历写入数据库，区分了工作日，节假日，周末",notes = "每天api最多请求五次")
    @PostMapping("/writeData")
    public ServerResponse create() throws IOException {
        HolidayVo holidayVo = new HolidayVo();
        // 判断是否是新年份数据
        int currentYear = LocalDate.now().getYear();
        System.out.println("currentYear = " + currentYear);
        String date = holidayVo.getDate();
        if (date != null) {
            try {
                int year = Integer.parseInt((date.substring(0, 4)));
                if (year > currentYear) {
                    // 新年份数据，清空原有数据
                    holidayVoService.truncateTableAndResetId();
                } else {
                    // 非新年份数据，根据年份查询是否已存在相同年份的数据
                    int count = holidayVoService.countByYear(holidayVo.getDate());
                    if (count > 0) {
                        return ServerResponse.ofError("已存在相同年份的数据，不再写入。");
                    }
                }
            } catch (NumberFormatException e) {
                throw new RuntimeException(e);
            }
        } else {
            holidayVoService.truncateTableAndResetId();
            ArrayList<HolidayVo> HolidayVoList = HolidayUtil.getAllHolidayByYear(currentYear);

            boolean success = false;
            for (HolidayVo HolidayVo : HolidayVoList) {
                System.err.println(HolidayVo);
                success = holidayVoService.save(HolidayVo);
            }
            if (success) {
                return ServerResponse.ofSuccess(holidayVoService.list());
            } else {
                return ServerResponse.ofError("数据写入失败。");
            }

        }
        return ServerResponse.ofSuccess("写入年份"+currentYear);
    }
    @ApiOperation(value = "查询全年日历")

    @GetMapping("/getData")
    public ServerResponse getPage(@RequestParam(required = false) String date) {
        ServerResponse response = new ServerResponse();

        if (date != null) {
            QueryWrapper<HolidayVo> wrapper = new QueryWrapper<>();
            wrapper.like("date", date);

            List<HolidayVo> holidayList = holidayVoService.list(wrapper); // 执行基于日期的查询
            response.setData(holidayList);
        } else {
            response.setData(holidayVoService.list()); // 执行不带日期的查询
        }

        response.setMessage("查询成功");
        return response;
    }

    @GetMapping("/getData/{date}")
    public ServerResponse getPageByDate(@PathVariable String date) {
        ServerResponse response = new ServerResponse();

        QueryWrapper<HolidayVo> wrapper = new QueryWrapper<>();
        wrapper.like("date", date);

        List<HolidayVo> holidayList = holidayVoService.list(wrapper); // 执行基于日期的查询
        response.setData(holidayList);

        response.setMessage("查询成功");
        return response;
    }



    @ApiOperation(value = "根据id查询假期，方便更新操作")
    @GetMapping("/{id}")
    public ServerResponse get(@PathVariable Integer id) {
        return ServerResponse.ofSuccess(holidayVoService.getById(id));
    }
    @ApiOperation(value = "修改全年日历",notes = "status:0 工作日，1：周末，2：法定节假日")
    @PutMapping
    public ServerResponse update(@RequestBody @Validated HolidayVo entity) {
        return ServerResponse.ofSuccess(holidayVoService.update(entity));
    }
    @ApiOperation(value = "根据id查询假期，方便更新操作")
    @GetMapping("/date/{date}")
    public ServerResponse selectLikeByDate(@PathVariable String date) {
        QueryWrapper<HolidayVo> wrapper = new QueryWrapper<>();
        wrapper.like("date", date);

        List<HolidayVo> holidayList = holidayVoService.list(wrapper); // 从数据库中提取查询结果

        return ServerResponse.ofSuccess(holidayList); // 返回提取的查询结果
    }

}
