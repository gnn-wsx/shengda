package cn.shen.sheng.da.controller;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.service.ScheduleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Api(tags = "课表信息接口")
@RestController
@RequestMapping("/schedule")
public class ScheduleController {
    @Autowired
    private ScheduleService service;
    @ApiOperation(value = "分页查询课表信息",notes = "默认第一页，输入学期：term")
    @GetMapping("/page/{term}")
    public ServerResponse scheduleByTerm(@PathVariable String term){
        return service.selectScheduleByTerm(1,term);
    }
    @ApiOperation(value = "分页查询班级信息，含班级人数",notes = "输入要查询页数，index，要查询学期：term")
    @GetMapping("/page/{term}/{index}")
    public ServerResponse scheduleByTerm(@PathVariable String term,@PathVariable Integer index){
        return service.selectScheduleByTerm(index,term);
    }
}
