package cn.shen.sheng.da.controller;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.entity.StudentEntity;
import cn.shen.sheng.da.model.vo.request.StudentLoginRequest;
import cn.shen.sheng.da.service.StudentService;
import cn.shen.sheng.da.service.impl.TokenService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
@Api(tags = "学生信息接口")
@RestController
@RequestMapping("/admin/student")
public class StudentController {
    @Autowired
    private StudentService service;
    @Autowired
    private TokenService tokenService;
    /**
     *  学生加入班级，只有加入班级后才可以看到本班的课表
     */
    @ApiOperation(value = "学生加入班级，只有加入班级后才可以看到本班的课表")
    @PostMapping("/join/{studentId}/{classId}")
    public ServerResponse joinClass(@PathVariable("studentId") Integer id, @PathVariable("classId") Integer class_id) {
        // TODO 学生加入年级，查看自己所在的班级课表
        StudentEntity student = service.getById(id);
        student.setClassId(class_id);
        boolean b = service.saveOrUpdate(student);
        if (b) {
            return ServerResponse.ofSuccess("加入班级成功");
        }
        return ServerResponse.ofError("加入班级失败");
    }
    /**
     * 学生登录
             */
    @ApiOperation(value = "学生登录",notes = "用token验证")
    @PostMapping("/login")
    public ServerResponse studentLogin(@RequestBody StudentLoginRequest studentLoginRequest) {
        Map<String, Object> map = new HashMap<>();
        // 先判断是否有该学号，该学生
        QueryWrapper<StudentEntity> wrapper = new QueryWrapper<StudentEntity>().eq("student_number", studentLoginRequest.getStudentNumber());
        // 查询是否有该学生
        StudentEntity student2 = service.getOne(wrapper);

        if (student2 == null) {
            return ServerResponse.ofError("学生账号不存在!");

        }
        // 调用登录
        StudentEntity student = service.studentLogin(studentLoginRequest.getStudentNumber(), studentLoginRequest.getStudentPassword());
        if (student != null) {
            //允许登录,返回token
            String token = tokenService.getToken(student);
            map.put("student", student);
            map.put("token", token);
            return ServerResponse.ofSuccess(map);
        }
        return ServerResponse.ofSuccess("密码错误！");
    }
    @ApiOperation(value = "分页查询学生信息",notes = "输入要查询页数，index")
    @GetMapping("/studentInfo/{index}")
    public ServerResponse studentInfo(@PathVariable("index") Integer index,
                                      @RequestParam(defaultValue = "10") Integer limit){
        QueryWrapper<StudentEntity> studentEntityQueryWrapper = new QueryWrapper<>();
        studentEntityQueryWrapper.orderByAsc("student_id");
        Page<StudentEntity> studentEntityPage = new Page<>(index,limit);
        IPage<StudentEntity> iPage = service.page(studentEntityPage,studentEntityQueryWrapper);
        return ServerResponse.ofSuccess(iPage.getRecords());

    }
    @ApiOperation(value = "分页查询学生信息",notes = "默认第一页")
    @GetMapping("/studentInfo")
    public ServerResponse studentInfo(@RequestParam(defaultValue = "10") Integer limit){
        QueryWrapper<StudentEntity> studentEntityQueryWrapper = new QueryWrapper<>();
        studentEntityQueryWrapper.orderByAsc("student_id");
        Page<StudentEntity> studentEntityPage = new Page<>(1,limit);
        IPage<StudentEntity> iPage = service.page(studentEntityPage,studentEntityQueryWrapper);
        return ServerResponse.ofSuccess(iPage.getRecords());

    }
    @ApiOperation(value = "统计学生分页下标页码")
    @GetMapping("/page/count")
    public ServerResponse getPageCount(){
        int count = service.count();
        return ServerResponse.ofSuccess(count);
    }
    @ApiOperation(value = "新增学生")
    @PostMapping
    public ServerResponse create(@RequestBody @Validated StudentEntity entity) {
        return ServerResponse.ofSuccess(service.save(entity));
    }
    @ApiOperation(value = "删除学生",notes = "根据studentID删除")

    @DeleteMapping("/{studentID}")
    public ServerResponse deleteItem(@PathVariable Integer studentID) {
        return ServerResponse.ofSuccess(service.removeById(studentID));
    }
    @ApiOperation(value = "根据studentID查询学生，方便更新操作")
    @GetMapping("/{studentID}")
    public ServerResponse get(@PathVariable Integer studentID) {
        return ServerResponse.ofSuccess(service.getById(studentID));
    }
    @ApiOperation(value = "更新学生信息")
    @PutMapping
    public ServerResponse update(@RequestBody @Validated StudentEntity entity) {
        return ServerResponse.ofSuccess(service.update(entity));
    }
    @ApiOperation(value = "模糊查询学生信息",notes = "根据学号：username")
    @GetMapping("/getPageCountByStudentId/{username}")
    public ServerResponse getPageCountByStudentId(@PathVariable Integer username, @RequestParam(defaultValue = "10") Integer limit){
        QueryWrapper<StudentEntity> studentEntityQueryWrapper = new QueryWrapper<>();
        studentEntityQueryWrapper.like("student_number",username);
        Page<StudentEntity> pages = new Page<>(1, limit);
        IPage<StudentEntity> iPage = service.page(pages, studentEntityQueryWrapper);
        if (iPage.getRecords() != null) {
            return ServerResponse.ofSuccess(iPage.getRecords());
        }
        return ServerResponse.ofError("查询失败!");
    }







}
