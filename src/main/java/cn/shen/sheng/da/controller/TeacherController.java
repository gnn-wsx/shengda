package cn.shen.sheng.da.controller;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.entity.TeacherEntity;
import cn.shen.sheng.da.model.vo.request.TeacherAddRequest;
import cn.shen.sheng.da.model.vo.request.TeacherLoginRequest;
import cn.shen.sheng.da.model.vo.response.PasswordVO;
import cn.shen.sheng.da.service.TeacherService;
import cn.shen.sheng.da.service.impl.TokenService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
@Api(tags = "教师信息接口")

@RestController
@RequestMapping("/admin/teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private TokenService tokenService;
    /**
     * 讲师登录
     */
    @ApiOperation(value = "讲师登录",notes = "token验证")
    @PostMapping("/login")
    public ServerResponse teacherLogin(@RequestBody TeacherLoginRequest teacherLoginRequest) {
        Map<String, Object> map = new HashMap<>();
        QueryWrapper<TeacherEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("teacher_number", teacherLoginRequest.getTeacherNumber());
        // 先查询是否有该账号
        TeacherEntity teacher2 = teacherService.getOne(wrapper);
        if (teacher2 == null) {
            return ServerResponse.ofError("账号不存在");
        }
        // 登录,使用编号登录
        TeacherEntity teacher = teacherService.teacherLogin(teacherLoginRequest.getTeacherNumber(), teacherLoginRequest.getTeacherPassword());
        if (teacher != null) {
            // 允许登录
            String token = tokenService.getToken(teacher);
            map.put("teacher", teacher);
            map.put("token", token);
            return ServerResponse.ofSuccess(map);
        }
        // 否则一律视为密码错误
        return ServerResponse.ofError("密码错误");
    }

    /**
     * 根据id查询讲师，用于更新操作
     */
    @ApiOperation(value = "根据teacherId查询讲师，用于更新操作")
    @GetMapping("/{teacherId}")
    public ServerResponse queryTeacherById(@PathVariable("teacherId") Integer teacher_id) {

        return ServerResponse.ofSuccess(teacherService.getById(teacher_id));
    }

    /**
     * 更新讲师
     * @param teacher
     * @return
     */
    @ApiOperation(value = "根据teacherId更新讲师")
    @PostMapping("/modify/{teacherId}")
    public ServerResponse modifyTeacher(@PathVariable("teacherId") Integer id,@RequestBody TeacherEntity teacher) {
        QueryWrapper<TeacherEntity> teacherEntityQueryWrapper = new QueryWrapper<TeacherEntity>().eq("teacher_id",id);
        boolean b = teacherService.update(teacher,teacherEntityQueryWrapper);
        if (b) {
            return ServerResponse.ofSuccess("更新成功");
        }
        return ServerResponse.ofError("更新失败");
    }

    /**
     * 分页查询讲师
     * @param index
     * @param limit
     * @return
     */
    @ApiOperation(value = "分页查询讲师",notes = "输入要查询页数index")
    @GetMapping("/page/{index}")
    public ServerResponse getPage(@PathVariable(value = "index") Integer index,
                                       @RequestParam(defaultValue = "10") Integer limit) {
        Page<TeacherEntity> pages = new Page<>(index, limit);
        QueryWrapper<TeacherEntity> wrapper = new QueryWrapper<TeacherEntity>().orderByAsc("teacher_id");
        IPage<TeacherEntity> iPage = teacherService.page(pages, wrapper);
        return ServerResponse.ofSuccess(iPage.getRecords());
    }

    /**
     * 根据姓名关键字搜索讲师
     * @return
     */
    @ApiOperation(value = "根据姓名关键字搜索讲师",notes = "输入要查询教师姓名teacherName")
    @GetMapping("/searchByName/{page}/{keyword}")
    public ServerResponse searchTeacher(@PathVariable("keyword") String keyword, @PathVariable("page") Integer page,
                                        @RequestParam(defaultValue = "10") Integer limit) {
        QueryWrapper<TeacherEntity> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("teacher_id");
        wrapper.like(!StringUtils.isEmpty(keyword), "teacher_name", keyword);
        Page<TeacherEntity> pages = new Page<>(page, limit);
        IPage<TeacherEntity> iPage = teacherService.page(pages, wrapper);
        if (page != null) {
            return ServerResponse.ofSuccess(iPage.getRecords());
        }
        return ServerResponse.ofError("查询失败!");
    }

    /**
     * 管理员根据ID删除讲师
     * @return
     */
    @ApiOperation(value = "管理员根据teacherId删除讲师",notes = "输入要删除教师工号：teacherId")
    @DeleteMapping("/delete/{teacherId}")
    public ServerResponse deleteTeacher(@PathVariable Integer teacherId) {
        boolean b = teacherService.removeById(teacherId);
        if(b) {
            return ServerResponse.ofSuccess("删除成功！");
        }
        return ServerResponse.ofError("删除失败！");
    }
    /**
     * 管理员添加讲师,默认密码是123456
     * @param t
     * @return
     */
    @ApiOperation(value = "管理员添加讲师",notes = "默认密码是123456")
    @PostMapping("/add")
    public ServerResponse addTeacher(@RequestBody TeacherAddRequest t) {
        TeacherEntity teacher = new TeacherEntity();
        teacher.setUsername(t.getTeacherNumber());
        teacher.setPassword("123456");
        teacher.setTeacherName(t.getTeacherName());
        teacher.setTeacherAbility(t.getTeacherAbility());
        teacher.setTeacherLevel(t.getTeacherLevel());
        boolean b = teacherService.save(teacher);
        if (b) {
            return ServerResponse.ofSuccess("添加讲师成功！");
        }
        return ServerResponse.ofError("添加讲师失败！");
    }
    /**
     * 修改密码
     * @param passwordVO
     * @return
     */
    @ApiOperation(value = "修改讲师密码")
    @PostMapping("/password")
    public ServerResponse updatePass(@RequestBody PasswordVO passwordVO) {
        QueryWrapper<TeacherEntity> wrapper = new QueryWrapper();
        wrapper.eq("teacher_id", passwordVO.getTeacherId());
        wrapper.eq("teacher_password", passwordVO.getOldPass());
        TeacherEntity teacher = teacherService.getOne(wrapper);
        if (teacher == null) {
            return ServerResponse.ofError("旧密码错误");
        }
        // 否则进入修改密码流程
        teacher.setPassword(passwordVO.getNewPass());
        boolean b = teacherService.updateById(teacher);
        if (b) {
            return ServerResponse.ofSuccess("密码修改成功");
        }
        return ServerResponse.ofError("密码更新失败");
    }

    /**
     * 查询所有讲师
     * @return
     */
    @ApiOperation(value = "查询所有讲师")
    @GetMapping("/all")
    public ServerResponse getAllTeacher() {

        return ServerResponse.ofSuccess(teacherService.list());
    }
    @ApiOperation(value = "分页查询返回班级",notes = "输入要查询页数index")
    @GetMapping("/InfoList/{index}")
    public ServerResponse getAllTeacherInfo( @PathVariable Integer index) {

        return teacherService.teacherInfoList(index);
    }
    @ApiOperation(value = "统计教师分页下标页码")
    @GetMapping("/page/count")
    public ServerResponse getPageCount(){
        int count = teacherService.count();
        return ServerResponse.ofSuccess(count);
    }



}
