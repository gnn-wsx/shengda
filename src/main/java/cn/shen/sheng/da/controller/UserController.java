package cn.shen.sheng.da.controller;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.vo.response.LoginVO;
import cn.shen.sheng.da.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Api(tags = "用户登录接口")
@RequestMapping("/user")
@RestController
public class UserController  {
    @Autowired
    private UserService service;

    public UserController(UserService service) {
        this.service = service;
    }
    @ApiOperation(value = "用户登录",notes = "学生type=1，教师type=2，管理员type=3")
    @PostMapping("/login")
    public ServerResponse login(@Validated @RequestBody LoginVO loginVO) {
        System.out.println("loginVO = " + loginVO);
        String username = loginVO.getUsername();
        String password = loginVO.getPassword();
        Integer userType = loginVO.getUserType();
        return service.login(username, password, userType);
    }
    @ApiOperation(value = "用session判断登录状态")

    @RequestMapping("/login/status")
    public ServerResponse getLoginStatus() {
        return service.getLoginStatus();
    }
    @ApiOperation(value = "用户注销、登出")

    @RequestMapping("/logout")
    public ServerResponse logout() {
        return service.logout();
    }
}
