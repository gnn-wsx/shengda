package cn.shen.sheng.da.controller.student;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.service.student.TimetableServiceS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Api(tags = "查询学生课表")
@RequestMapping("/student/timetable")
@RestController("student_timeTableController")
public class TimetableController  {
    private final TimetableServiceS service;

    public TimetableController(TimetableServiceS service) {
        this.service = service;
    }
    @ApiOperation(value = "登录时传过来id",notes = "根据传过来id查询学生课表")
    @RequestMapping
    public ServerResponse get() {
        return ServerResponse.ofSuccess(service.get());
    }
}
