package cn.shen.sheng.da.controller.teacher;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.service.teacher.TimetableServiceT;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Api(tags = "查询教师课表")
@RequestMapping("/teacher/timetable")
@RestController
public class TimetableController  {
    private final TimetableServiceT service;

    public TimetableController(TimetableServiceT service) {
        this.service = service;
    }
    @ApiOperation(value = "登录时传过来id",notes = "根据传过来id查询教师课表")
    @RequestMapping
    public ServerResponse get() {
        return ServerResponse.ofSuccess(service.get());
    }
}