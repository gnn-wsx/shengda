package cn.shen.sheng.da.manager;

import cn.shen.sheng.da.mapper.CourseMapper;
import cn.shen.sheng.da.mapper.TeacherMapper;
import cn.shen.sheng.da.model.vo.response.TeacherVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CourseDao {
    private final CourseMapper course;
    @Autowired
    private final TeacherMapper teacher;

    public CourseDao(CourseMapper course, TeacherMapper teacher) {
        this.course = course;
        this.teacher = teacher;
    }


    public List<TeacherVO> getTeachersByCourseName(String courseName) {

        int courseLevel = course.getCourseLevelByName(courseName);
        List<TeacherVO> teachers;

        if (courseLevel >= 0 && courseLevel <= 10) {
            teachers = teacher.getTeachersByCourseLevel(1);
        } else if (courseLevel >= 11 && courseLevel <= 20) {
            teachers = teacher.getTeachersByCourseLevel(2);
        } else if (courseLevel >= 21 && courseLevel <= 30) {
            teachers = teacher.getTeachersByCourseLevel(3);
        } else {
            teachers = teacher.getTeachersByCourseLevel(0);
        }

        return teachers;
    }
}
