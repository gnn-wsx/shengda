package cn.shen.sheng.da.manager;

import cn.shen.sheng.da.model.bo.LoginStatusBO;
import cn.shen.sheng.da.service.impl.LoginStatusManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
@Component
public class StudentLoginStatusDao {
    @Autowired
    private HttpSession session;
    @Autowired
    private LoginStatusManager loginStatusManager;

    private LoginStatusBO getLoginStatus() {
        return loginStatusManager.getLoginStatus(session);
    }

    public Integer getUserId() {
        return getLoginStatus().getUserId();
    }

}
