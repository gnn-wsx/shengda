package cn.shen.sheng.da.manager;

import cn.shen.sheng.da.mapper.AdminMapper;
import cn.shen.sheng.da.mapper.StudentMapper;
import cn.shen.sheng.da.mapper.TeacherMapper;
import cn.shen.sheng.da.model.entity.AdminEntity;
import cn.shen.sheng.da.model.entity.StudentEntity;
import cn.shen.sheng.da.model.entity.TeacherEntity;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Component;

@Component
public class UserDAO {
    private final AdminMapper adminDAO;
    private final TeacherMapper teacherDAO;
    private final StudentMapper studentDAO;

    public UserDAO(AdminMapper adminDAO, TeacherMapper teacherDAO, StudentMapper studentDAO) {
        this.adminDAO = adminDAO;
        this.teacherDAO = teacherDAO;
        this.studentDAO = studentDAO;
    }

    public StudentEntity getByNumber1(String number) {
        LambdaQueryWrapper<StudentEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StudentEntity::getUsername, number);

        return studentDAO.selectOne(wrapper);
    }
    public TeacherEntity getByNumber2(String number) {
        LambdaQueryWrapper<TeacherEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TeacherEntity::getUsername, number);
        return teacherDAO.selectOne(wrapper);
    }
    public AdminEntity getByNumber3(String number) {
        LambdaQueryWrapper<AdminEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(AdminEntity::getUsername, number);
        return adminDAO.selectOne(wrapper);
    }

}
