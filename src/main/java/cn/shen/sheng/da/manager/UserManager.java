package cn.shen.sheng.da.manager;

import cn.shen.sheng.da.mapper.StudentMapper;
import cn.shen.sheng.da.model.bo.AuthInfoBO;
import cn.shen.sheng.da.model.entity.StudentEntity;
import cn.shen.sheng.da.model.vo.request.UserType;
import org.springframework.stereotype.Component;

@Component
public class UserManager {
    private final UserDAO userManager;

    private final StudentMapper studentDAO;

    public UserManager(UserDAO userManager, StudentMapper studentDAO) {
        this.userManager = userManager;
        this.studentDAO = studentDAO;
    }


    public AuthInfoBO getAuthInfoByUsername(String username, Integer userType) {
        if (userType == UserType.STUDENT) {
            return AuthInfoBO.fromStudent(userManager.getByNumber1(username));
        } else if (userType == UserType.TEACHER) {
            return AuthInfoBO.fromTeacher(userManager.getByNumber2(username));
        } else if (userType == UserType.ADMIN) {
            return AuthInfoBO.fromAdmin(userManager.getByNumber3(username));
        }

        return null;
    }
    public void updateStudentLastLoginTime(String number) {
        StudentEntity entity = userManager.getByNumber1(number);
        if (entity == null) {
            return;
        }


        studentDAO.updateById(entity);
    }
}
