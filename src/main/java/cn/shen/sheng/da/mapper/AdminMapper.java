package cn.shen.sheng.da.mapper;

import cn.shen.sheng.da.model.entity.AdminEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminMapper extends BaseMapper<AdminEntity> {

}
