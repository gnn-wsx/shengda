package cn.shen.sheng.da.mapper;

import cn.shen.sheng.da.model.entity.ClassEntity;
import cn.shen.sheng.da.model.vo.response.ClassInfoVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ClassMapper extends BaseMapper<ClassEntity> {
    @Select("SELECT\n" +
            "    c.class_id,\n" +
            "    c.class_name,\n" +
            "    COUNT(s.student_id) AS count3,\n" +
            "    c.room\n" +
            "FROM\n" +
            "    classes AS c\n" +
            "LEFT JOIN\n" +
            "    students AS s ON c.class_id = s.class_id\n" +
            "GROUP BY\n" +
            "    c.class_id")
    IPage<ClassInfoVO> classInfo(IPage<ClassInfoVO> page,@Param(Constants.WRAPPER) QueryWrapper<ClassInfoVO> queryWrapper);
    @Select("select count(class_id) from classes")
    Integer count();
    @Select("select c.class_id,c.class_name,count(*) as count3 ,c.room from classes as c inner join students as s where c.class_id=s.class_id GROUP BY class_id")
    List<ClassInfoVO> classInfo2();
    @Select("SELECT distinct class_id FROM classes")
    List<String> selectClassNo();
    @Select("SELECT * FROM classes")
    List<ClassEntity> allClasses();
    @Select("select * from classes where class_id = #{class_id}")
    ClassEntity allClassesById(@Param("class_id" ) Integer classId);
    @Select("select distinct ${columnName} from classes")
    List<String> selectByColumnName(@Param("columnName") String columnName);




}
