package cn.shen.sheng.da.mapper;

import cn.shen.sheng.da.model.entity.CourseEntity;
import cn.shen.sheng.da.model.vo.response.StudentCourseVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CourseMapper extends BaseMapper<CourseEntity> {
    @Select("select count(course_id) from courses")
    Integer count();
    @Select("select c.course_name,t.teacher_name,c.course_status ,c.term,c2.room,s.is_completed,s.class_weeks from courses as c inner join schedules s on c.course_id = s.course_id inner join classes c2 on s.class_id = c2.class_id INNER JOIN teachers AS t ON t.teacher_number = s.teacher_number where c2.class_id =  #{class_id}  ")
    IPage<StudentCourseVO> selectStudentCourse(@Param("class_id") Integer class_id, Page<StudentCourseVO> page, @Param(Constants.WRAPPER) QueryWrapper<StudentCourseVO> queryWrapper);
    @Select("SELECT course_level FROM courses WHERE course_name = #{course_name}")
    int getCourseLevelByName(@Param("course_name") String courseName);
    @Select("SELECT * FROM courses WHERE term = #{term}")
    List<CourseEntity> allCourse(@Param("term") String term);
    @Select("select count(course_id) from courses where term = #{term}")
    Integer count1(@Param("term") String term);






}
