package cn.shen.sheng.da.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.time.LocalDate;

@Mapper
public interface CoursePlanMapper {
    @Update("truncate table scheduleinfo")
    void deleteSchedule();
    @Insert("insert into scheduleinfo(class_id, course_id, class_time, teacher_number, start_date, end_date,class_weeks,is_completed) values( #{class_id}, #{course_id}, #{class_time}, #{teacher_id},#{start_date},#{start_date},#{class_weeks},#{is_completed})")
    void insertCoursePlan(@Param("class_id") String class_id, @Param("course_id") String course_id, @Param("class_time") String class_time,
                          @Param("teacher_number") Integer teacher_number , @Param("start_date") LocalDate start_date, @Param("end_date") LocalDate end_date, @Param("class_weeks") String class_weeks, @Param("is_completed") Integer is_completed);
}
