package cn.shen.sheng.da.mapper;

import cn.shen.sheng.da.model.entity.HolidayVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface HolidayVoMapper extends BaseMapper<HolidayVo> {
    @Select("SELECT COUNT(*) FROM holiday WHERE date = #{year}")
    int countByYear(int year);
    @Select("select h.* from holiday as h ")
    List<HolidayVo> allHoliday();
    @Select("select h.* from holiday as h where  h.date > #{classDates}")
    List<HolidayVo> allHoliday1(@Param("classDates") LocalDate startDate);
    @Delete("TRUNCATE TABLE holiday")
    void truncateTable();
    @Update("ALTER TABLE holiday AUTO_INCREMENT = 1") // 重置自增主键的序号（适用于 MySQL）
    void resetAutoIncrement();

}
