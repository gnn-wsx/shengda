package cn.shen.sheng.da.mapper;

import cn.shen.sheng.da.model.entity.ScheduleEntity;
import cn.shen.sheng.da.model.vo.response.TermScheduleVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.*;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface ScheduleMapper extends BaseMapper<ScheduleEntity> {
    @Select("select c.course_id,c.term,c2.class_name,c2.room,c.course_name, c.hours " +
            "from courses as c inner join schedules s on c.course_id = s.course_id " +
            "inner join classes c2 on s.class_id = c2.class_id " +
            "where c.course_status = 0 and c.term = #{term} " +
            "order by c.course_order")
    List<TermScheduleVO> selectScheduleByTerm(Page<TermScheduleVO> page, @Param("term") String term);
    @Update("truncate table schedules")
    void deleteSchedule();
    @Insert("insert into schedules(class_id, course_id, class_time, teacher_number, start_date, end_date,class_weeks,is_completed,room) values( #{class_id}, #{course_id}, #{class_time}, #{teacher_number},#{start_date},#{end_date},#{class_weeks},#{is_completed},#{room})")
    void insertCoursePlan(@Param("class_id") String class_id, @Param("course_id") String course_id, @Param("class_time") String class_time, @Param("teacher_number") Integer teacher_number , @Param("start_date") LocalDate start_date, @Param("end_date") LocalDate end_date,  @Param("class_weeks") String class_weeks, @Param("is_completed") Integer is_completed,@Param("room") String room);

}
