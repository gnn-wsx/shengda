package cn.shen.sheng.da.mapper;

import cn.shen.sheng.da.model.entity.StudentEntity;
import cn.shen.sheng.da.model.vo.response.ClassVO;
import cn.shen.sheng.da.model.vo.response.StudentInfoVO;
import cn.shen.sheng.da.model.vo.response.StudentScheduleVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface StudentMapper extends BaseMapper<StudentEntity> {
    @Select("select count(student_id) from students")
    Integer count();
    @Select("select student_id,student_number,student_name,c.class_name,c.room from students inner join classes c on students.class_id = c.class_id order by c.class_id")
    List<StudentInfoVO> selectStudentInfo(Page<StudentInfoVO> page);
    @Select("select course_name,course_id,course_status,teacher_name,room,class_time from courseplan where class_id = #{class_id}")
    List<StudentScheduleVO> listStudentTimetable(@Param("class_id")Integer classId);
    @Select("select class_id from students where student_id = #{student_id}")
    ClassVO selectClass(@Param("student_id")Integer studentId);
}
