package cn.shen.sheng.da.mapper;

import cn.shen.sheng.da.model.entity.TeacherEntity;
import cn.shen.sheng.da.model.vo.response.TeacherInfoVO;
import cn.shen.sheng.da.model.vo.response.TeacherScheduleVO;
import cn.shen.sheng.da.model.vo.response.TeacherVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface TeacherMapper extends BaseMapper<TeacherEntity> {
    @Select("select count(teacher_id) from teachers")
    Integer count();
    @Select("SELECT t.teacher_name,t.teacher_ability ,t.teacher_id,t.teacher_number FROM teachers as t WHERE teacher_level = #{teacher_level}")
    List<TeacherVO> getTeachersByCourseLevel(@Param("teacher_level") Integer teacher_level);
    @Select("select teacher_id,teacher_number,teacher_name,teacher_ability from teachers")
    IPage<TeacherInfoVO> selectTeacherInfo(Page<TeacherInfoVO> page,@Param(Constants.WRAPPER) QueryWrapper<TeacherInfoVO> queryWrapper);
    @Select("select * from teachers")
    List<TeacherEntity> allTeacher();
    @Select("select course_name,course_id,course_status,teacher_name,room,class_time,class_id from courseplan where teacher_id = #{teacher_id}")
    List<TeacherScheduleVO> listStudentTimetable(@Param("teacher_id")Integer teacherId);




}
