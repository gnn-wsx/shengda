package cn.shen.sheng.da.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("admin")
public class AdminEntity {
    public static final String ID = "id";
    public static final String USERNAME = "admin_username";
    public static final String PASSWORD = "admin_password";
    public static final String LEVEL = "permission_level";

    @TableId(value = ID, type = IdType.AUTO)
    private Integer id;

    @TableField(USERNAME)
    private String username;

    @TableField(PASSWORD)
    private String password;
    @TableField(LEVEL)
    private Integer level;
}


