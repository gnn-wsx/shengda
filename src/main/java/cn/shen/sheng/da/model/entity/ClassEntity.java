package cn.shen.sheng.da.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("classes")
public class ClassEntity {
    public static final String CLASSID = "class_id";
    public static final String CLASSNAME = "class_name";
    public static final String ROOM = "room";

    @TableId(value = CLASSID, type = IdType.AUTO)
    private Integer classId;

    @TableField(CLASSNAME)
    private String className;

    @TableField(ROOM)
    private String room;
}
