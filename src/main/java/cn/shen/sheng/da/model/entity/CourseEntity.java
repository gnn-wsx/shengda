package cn.shen.sheng.da.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("courses")
public class CourseEntity {
    public static final String ID = "course_id";
    public static final String NAME = "course_name";
    public static final String HOURS = "hours";
    public static final String ORDER = "course_order";
    public static final String STATUS = "course_status";
    public static final String TERM = "term";
    public static final String LEVEL = "course_level";
    public static final String FAX = "isFax";

    @TableId(value = ID, type = IdType.AUTO)
    private Integer courseId;
    @TableField(NAME)
    private String courseName;

    @TableField(HOURS)
    private Integer hours;
    @TableField(ORDER)
    private Integer courseOrder;
    @TableField(STATUS)
    private Integer courseStatus;
    @TableField(TERM)
    private String term;
    @TableField(LEVEL)
    private Integer courseLevel;
    @TableField(FAX)
    private Integer isFax;

}
