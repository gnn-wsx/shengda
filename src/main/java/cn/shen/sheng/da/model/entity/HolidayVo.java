package cn.shen.sheng.da.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("holiday")
public class HolidayVo {
    public static final String ID = "id";
    public static final String DATE = "date";
    public static final String STATUS = "status";
    public static final String MESSAGE = "message";

    @TableId(value = ID, type = IdType.AUTO)
    private Long id;

    @TableField(DATE)
    private String date;

    @TableField(STATUS)
    private Integer status;

    @TableField(MESSAGE)
    private String message;
}
