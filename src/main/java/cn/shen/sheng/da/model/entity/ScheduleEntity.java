package cn.shen.sheng.da.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;

@Data
@TableName("schedules")
public class ScheduleEntity {
    public static final String ID = "schedule_id";
    public static final String CLASS_ID = "class_id";
    public static final String TEACHER_NUMBER = "teacher_number";
    public static final String COURSE_ID = "course_id";
    public static final String START_DATE = "start_date";
    public static final String END_DATE = "end_date";
    public static final String CLASS_TIME = "class_time";
    public static final String CLASS_WEEKS = "class_weeks";
    public static final String IS_COMPLETED = "is_completed";
    public static final String IS_FIX = "isFix";
    public static final String ROOM = "room";




    @TableId(value = ID, type = IdType.AUTO)
    private Integer id;

    @TableField(CLASS_ID)
    private Integer classId;
    @TableField(IS_FIX)
    private Integer isFix;

    @TableField(TEACHER_NUMBER)
    private Integer teacherNumber;

    @TableField(COURSE_ID)
    private Integer courseId;

    @TableField(START_DATE)
    private LocalDate startDate;
    @TableField(END_DATE)
    private LocalDate endDate;

    @TableField(CLASS_TIME)
    private String classTime;
    @TableField(CLASS_WEEKS)
    private Integer class_weeks;

    @TableField(IS_COMPLETED)
    private Integer isCompleted;
    @TableField(ROOM)
    private Integer room;

}

