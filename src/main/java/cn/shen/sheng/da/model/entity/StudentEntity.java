package cn.shen.sheng.da.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("students")
public class StudentEntity {
    public static final String ID = "student_id";
    public static final String NUMBER = "student_number";
    public static final String NAME = "student_name";
    public static final String PASSWORD = "student_password";
    public static final String CLASS_ID = "class_id";

    @TableId(value = ID, type = IdType.AUTO)
    private Integer studentId;
    @TableField(NUMBER)
    private String username;
    @TableField(NAME)
    private String studentName;

    @TableField(PASSWORD)
    private String password;

    @TableField(CLASS_ID)
    private Integer classId;
}
