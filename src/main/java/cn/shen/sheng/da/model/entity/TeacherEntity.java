package cn.shen.sheng.da.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("teachers")
public class TeacherEntity {
    public static final String ID = "teacher_id";
    public static final String NAME = "teacher_name";
    public static final String NUMBER = "teacher_number";
    public static final String PASSWORD = "teacher_password";
    public static final String ABILITY = "teacher_ability";
    public static final String LEVEL = "teacher_level";



    @TableId(value = ID, type = IdType.AUTO)
    private Integer teacherId;

    @TableField(NAME)
    private String teacherName;
    @TableField(NUMBER)
    private String username;
    @TableField(PASSWORD)
    private String password;

    @TableField(ABILITY)
    private String teacherAbility;
    @TableField(LEVEL)
    private String teacherLevel;


}

