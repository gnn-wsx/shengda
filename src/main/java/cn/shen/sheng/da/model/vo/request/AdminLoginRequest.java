package cn.shen.sheng.da.model.vo.request;

import lombok.Data;

/**
 * @author: 15760
 * @Date: 2020/3/14
 * @Descripe: 封装用户(管理员，讲师)登录请求体
 */
@Data
public class AdminLoginRequest {

    private String adminUsername;

    private String adminPassword;
    private Integer userType;

}
