package cn.shen.sheng.da.model.vo.request;

/**
 * @author: 15760
 * @Date: 2020/3/20
 * @Descripe: 定义属性,方便切割编码的时候选择
 */
public class ConstantInfo {

    // 是否固定上课时间 1位
    public static final String IS_FIX = "isFix";


    // 班级编号 位
    public static final String CLASS_ID = "class_id";

    // 教师编号  位
    public static final String TEACHER_NUMBER = "teacher_number";

    // 课程编号  位
    public static final String COURSE_ID = "course_id";

    // 教室编号6位
    public static final String ROOM = "room";

    // 上课时间2位
    public static final String CLASS_TIME = "class_time";

    // 开课学期
    public static final String TERM = "term";


    // 默认课程的编码
    public static final String DEFAULT_CLASS_TIME = "00";

    // 设置遗传代数
    public static final int GENERATION = 100;

}
