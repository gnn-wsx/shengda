package cn.shen.sheng.da.model.vo.request;

import lombok.Data;

/**
 * @Descripe: 封装学生登录的请求体
 */
@Data
public class StudentLoginRequest {

    private String studentNumber;

    private String studentPassword;
    private String userType;
}
