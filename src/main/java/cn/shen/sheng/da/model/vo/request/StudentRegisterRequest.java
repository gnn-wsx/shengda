package cn.shen.sheng.da.model.vo.request;

import lombok.Data;

/**
 * @Descripe: 封装学生注册的信息
 */
@Data
public class StudentRegisterRequest {

    // 学号由系统给学生生成，学生通过完善个人信息进行填写其它字段
    private String studentName;

    private String studentNumber;

    private String studentPassword;



}
