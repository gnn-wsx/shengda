package cn.shen.sheng.da.model.vo.response;

import cn.shen.sheng.da.model.entity.CourseEntity;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CourseInfoVO extends CourseEntity {
    private Integer courseOrder;
    private String term;
    private String classId;
    private Integer courseId;
    private String courseName;
    private Integer teacherId;
    private Integer teacherNumber;
    private String teacherName;
    private String teacherAbility;
    private Integer teacherLevel;
    private Integer hours;
    private String room;
    private Integer courseStatus;
    private LocalDate startDate;
    private LocalDate endDate;
    private String classTime;
    private String classWeeks;
    private Integer isCompleted;
    private Integer isFax;



}
