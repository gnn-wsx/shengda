package cn.shen.sheng.da.model.vo.response;

import lombok.Data;

/**
 * @author: 15760
 * @Date: 2020/5/30
 * @Descripe:
 */
@Data
public class PasswordVO {

    private Integer teacherId;

    private String oldPass;

    private String newPass;

    private String rePass;
}
