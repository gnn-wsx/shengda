package cn.shen.sheng.da.model.vo.response;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class StudentCourseVO {
    private Integer courseOrder;
    private String term;
    private String courseName;
    private String teacherName;
    private String teacherAbility;
    private Integer teacherLevel;
    private Integer hours;
    private String room;
    private Integer courseStatus;
    private LocalDate startDate;
    private LocalDate endDate;
    private LocalTime classTime;
    private String classWeeks;
    private Integer isCompleted;



}
