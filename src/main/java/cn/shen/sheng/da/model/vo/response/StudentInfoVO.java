package cn.shen.sheng.da.model.vo.response;

import lombok.Data;

@Data
public class StudentInfoVO {
    private Integer studentId;
    private String studentNumber;
    private String studentName;
    private String className;
    private String room;


}
