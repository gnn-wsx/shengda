package cn.shen.sheng.da.model.vo.response;

import lombok.Data;

@Data
public class StudentScheduleVO {
    private String courseName;
    private String courseId;
    private String classId;
    private String room;
    private String teacherName;
    private String term;
    private Integer courseStatus;
    private String teacherAbility;
    private String classTime;

}
