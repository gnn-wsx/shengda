package cn.shen.sheng.da.model.vo.response;

import lombok.Data;

@Data
public class TeacherInfoVO {
    private Integer teacherId;
    private String teacherNumber;
    private String teacherName;
    private String teacherAbility;
}
