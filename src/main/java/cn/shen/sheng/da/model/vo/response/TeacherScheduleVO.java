package cn.shen.sheng.da.model.vo.response;

import lombok.Data;

@Data
public class TeacherScheduleVO {
    private String courseName;
    private Integer courseId;
    private String room;
    private Integer teacherId;
    private Integer teacherNumber;
    private String teacherName;
    private String className;
    private Integer hours;
    private String term;
    private String classTime;
    private String classId;
    private Integer courseStatus;
    private String teacherAbility;



}
