package cn.shen.sheng.da.model.vo.response;

import lombok.Data;

@Data
public class TeacherVO  {
    private Integer teacherId;
    private Integer teacherNumber;
    private String teacherName;
    private String teacherAbility;
    private Integer teacherLevel;
}
