package cn.shen.sheng.da.model.vo.response;

import lombok.Data;
@Data
public class TermScheduleVO {
    private Integer courseId;
    private String term;
    private String className;
    private String room;
    private String courseName;
    private String hours;


}
