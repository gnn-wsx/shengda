package cn.shen.sheng.da.service;


import cn.shen.sheng.da.model.entity.AdminEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component
public interface AdminService extends IService<AdminEntity> {
    AdminEntity adminLogin(@Param("username") String username, @Param("password") String password);



}
