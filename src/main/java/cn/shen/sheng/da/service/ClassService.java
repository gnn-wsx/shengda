package cn.shen.sheng.da.service;


import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.entity.ClassEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Component;
@Component
public interface ClassService extends IService<ClassEntity> {

    ServerResponse classInfo(Integer index);


    ServerResponse classSelectLike(String keyword, Integer index);
    int count();
    ServerResponse listName(Integer index);

    ServerResponse get(Integer id);



}
