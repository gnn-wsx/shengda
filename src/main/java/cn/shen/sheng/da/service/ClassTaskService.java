package cn.shen.sheng.da.service;

import cn.shen.sheng.da.common.ServerResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lequal
 * @since 2020-04-06
 */
public interface ClassTaskService {

//    Boolean classScheduling(ClassTask classTask);
    ServerResponse classScheduling(@Param("term") String term, List<String> classDates);

}
