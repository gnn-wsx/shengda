package cn.shen.sheng.da.service;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.entity.CourseEntity;
import cn.shen.sheng.da.model.vo.response.TeacherVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface CourseService extends IService<CourseEntity> {
    List<TeacherVO> getTeachersByCourseName(String courseName);


    ServerResponse selectStudentCourse1(Integer index, Integer classId);

    ServerResponse listName();
    ServerResponse selectCourseByPage(Integer index);
    int count();
    ServerResponse update(CourseEntity entity);
}
