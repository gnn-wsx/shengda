package cn.shen.sheng.da.service;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.entity.HolidayVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.ArrayList;

public interface HolidayVoService extends IService<HolidayVo> {
    int countByYear(String year);

    ArrayList<HolidayVo> save(ArrayList<HolidayVo> allHolidayByYear , int year);
    void truncateTableAndResetId();

    ServerResponse update(HolidayVo entity);
}
