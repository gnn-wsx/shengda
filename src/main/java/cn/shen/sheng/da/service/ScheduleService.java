package cn.shen.sheng.da.service;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.entity.ScheduleEntity;
import cn.shen.sheng.da.model.vo.response.TermScheduleVO;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ScheduleService extends IService<ScheduleEntity> {
    ServerResponse selectScheduleByTerm(Integer index, String term);
    List<ScheduleEntity> getClassInfoByPage(Integer index);
}
