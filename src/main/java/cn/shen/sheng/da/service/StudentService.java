package cn.shen.sheng.da.service;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.entity.StudentEntity;
import cn.shen.sheng.da.model.vo.response.StudentInfoVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface StudentService extends IService<StudentEntity> {
    ServerResponse allStudentInfo(Integer index);
    List<StudentInfoVO> studentSelectLikeByName(String keyword);
    List<StudentInfoVO> studentSelectLikeByNumber(String keyword);
    StudentEntity studentLogin(String number, String password);
    StudentEntity getByNumber(String name);
    List<StudentEntity> getClassInfoByPage(Integer index);
    int count();

    ServerResponse update(StudentEntity entity);
}
