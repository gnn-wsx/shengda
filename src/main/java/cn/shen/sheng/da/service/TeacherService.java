package cn.shen.sheng.da.service;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.entity.TeacherEntity;
import com.baomidou.mybatisplus.extension.service.IService;

public interface TeacherService extends IService<TeacherEntity> {
    ServerResponse teacherInfoList(Integer index);
    ServerResponse teacherInfoLikeByName(String keyword);
    ServerResponse teacherInfoLikeByNumber(String keyword);
    ServerResponse getClassInfoByPage(Integer index);
    TeacherEntity teacherLogin(String number, String password);
    int count();



}
