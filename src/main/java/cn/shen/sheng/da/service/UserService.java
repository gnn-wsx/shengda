package cn.shen.sheng.da.service;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.model.bo.AuthInfoBO;
import cn.shen.sheng.da.model.bo.LoginStatusBO;
import cn.shen.sheng.da.model.vo.request.UserType;
import cn.shen.sheng.da.service.impl.LoginStatusManager;
import cn.shen.sheng.da.manager.UserManager;
import cn.shen.sheng.da.util.Md5Encrypt;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service
public class UserService  {
    private static final String PASSWORD_SALT = "_Rain_Ng-_Azure_99";

    private final HttpSession session;
    private final LoginStatusManager loginStatusManager;
    private final UserManager manager;
    private final Md5Encrypt md5Encrypt;

    public UserService(HttpSession session, LoginStatusManager loginStatusManager, UserManager manager, Md5Encrypt md5Encrypt) {
        this.session = session;
        this.loginStatusManager = loginStatusManager;
        this.manager = manager;
        this.md5Encrypt = md5Encrypt;
    }


    public ServerResponse login(String username, String password, Integer userType) {
        AuthInfoBO authInfo = manager.getAuthInfoByUsername(username, userType);
        if (authInfo == null) {
            return ServerResponse.ofError("用户不存在");
        }
//        String passwordHash = computePasswordHash(password);
        if (!password.equals(authInfo.getPassword())) {
            return ServerResponse.ofError("密码错误");
        }

        if (authInfo.getUserType().equals(UserType.STUDENT)) {
            manager.updateStudentLastLoginTime(username);
        }

        LoginStatusBO statusBO = LoginStatusBO.fromAuthInfo(authInfo);
        loginStatusManager.setLoginStatus(session, statusBO);

        return ServerResponse.ofSuccess(statusBO);
    }

    public ServerResponse getLoginStatus() {
        LoginStatusBO statusBO = loginStatusManager.getLoginStatus(session);
        return ServerResponse.ofSuccess(statusBO);
    }

    public ServerResponse logout() {
        loginStatusManager.setLoginStatus(session, null);
        return ServerResponse.ofSuccess("注销成功");
    }

    public String computePasswordHash(String password) {
        String md5 = md5Encrypt.computeHexString(password);
        return md5Encrypt.computeHexString(md5 + PASSWORD_SALT);
    }
}
