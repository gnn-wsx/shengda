package cn.shen.sheng.da.service.impl;


import cn.shen.sheng.da.mapper.AdminMapper;
import cn.shen.sheng.da.model.entity.AdminEntity;
import cn.shen.sheng.da.service.AdminService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper,AdminEntity> implements AdminService {
    public static final int PAGE_SIZE = 20;
    @Autowired
    private AdminMapper adminMapper;

    public AdminEntity adminLogin(String username, String password) {
        QueryWrapper<AdminEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("admin_username", username);
        wrapper.eq("admin_password", password);
        return adminMapper.selectOne(wrapper);
    }
}
