package cn.shen.sheng.da.service.impl;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.mapper.ClassMapper;
import cn.shen.sheng.da.model.entity.ClassEntity;
import cn.shen.sheng.da.model.vo.response.ClassInfoVO;
import cn.shen.sheng.da.service.ClassService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassServiceImpl extends ServiceImpl<ClassMapper, ClassEntity> implements ClassService {
    public static final int PAGE_SIZE = 10;

    @Autowired
    private ClassMapper mapper;

    @Override
    public ServerResponse classInfo(Integer index) {
        QueryWrapper<ClassInfoVO> queryWrapper = new QueryWrapper<ClassInfoVO>().orderByAsc("class_id");
        Page<ClassInfoVO> page = new Page<>(index, PAGE_SIZE);
        List<ClassInfoVO> classList =  mapper.classInfo(page,queryWrapper).getRecords();
        System.out.println("classList = " + classList);
        return ServerResponse.ofSuccess(classList);
    }

    @Override
    public ServerResponse classSelectLike(String keyword , Integer index) {
        QueryWrapper<ClassInfoVO> queryWrapper = new QueryWrapper<ClassInfoVO>();
        queryWrapper.eq("class_name", keyword);
        System.out.println("queryWrapper = " + queryWrapper);
        Page<ClassInfoVO> page = new Page<>(index,PAGE_SIZE);
        IPage<ClassInfoVO> classList =  mapper.classInfo(page,queryWrapper.eq("class_name", keyword));
        ServerResponse response = new ServerResponse();
        response.setData(classList.getRecords());
        return ServerResponse.ofSuccess(response);
    }

    @Override
    public int count() {
        return mapper.count()/PAGE_SIZE+1;
    }

    @Override
    public ServerResponse listName(Integer index) {
        QueryWrapper<ClassEntity> queryWrapper = new QueryWrapper<ClassEntity>().orderByDesc("class_id");
        Page<ClassEntity> page = new Page<>(index, PAGE_SIZE);
        IPage<ClassEntity> iPage = mapper.selectPage(page,queryWrapper);
        return ServerResponse.ofSuccess(iPage.getRecords()) ;
    }



    @Override
    public ServerResponse get(Integer id) {
        ClassEntity entity = mapper.selectById(id);
        return ServerResponse.ofSuccess(entity);
    }
}
