package cn.shen.sheng.da.service.impl;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.mapper.CourseMapper;
import cn.shen.sheng.da.mapper.TeacherMapper;
import cn.shen.sheng.da.model.entity.CourseEntity;
import cn.shen.sheng.da.model.vo.response.StudentCourseVO;
import cn.shen.sheng.da.model.vo.response.TeacherVO;
import cn.shen.sheng.da.service.CourseService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, CourseEntity> implements CourseService {
    @Autowired
    private final CourseMapper course;
    @Autowired
    private final TeacherMapper teacher;
    public static final int PAGE_SIZE = 10;

    public CourseServiceImpl(CourseMapper course, TeacherMapper teacher) {
        this.course = course;
        this.teacher = teacher;
    }


    public List<TeacherVO> getTeachersByCourseName(String courseName) {

        int courseLevel = course.getCourseLevelByName(courseName);
        List<TeacherVO> teachers;

        if (courseLevel >= 0 && courseLevel <= 10) {
            teachers = teacher.getTeachersByCourseLevel(1);
        } else if (courseLevel >= 11 && courseLevel <= 20) {
            teachers = teacher.getTeachersByCourseLevel(2);
        } else if (courseLevel >= 21 && courseLevel <= 30) {
            teachers = teacher.getTeachersByCourseLevel(3);
        } else {
            teachers = teacher.getTeachersByCourseLevel(0);
        }

        return teachers;
    }

    @Override
    public ServerResponse selectStudentCourse1(Integer index, Integer classId) {
        Page<StudentCourseVO> page = new Page<>(index, PAGE_SIZE);
        QueryWrapper<StudentCourseVO> objectQueryWrapper = new QueryWrapper<StudentCourseVO>().orderByDesc("course_order");
        IPage<StudentCourseVO> coursePage = course.selectStudentCourse(classId,page, objectQueryWrapper);
        ServerResponse response = new ServerResponse();
        response.setData(coursePage);
        return response;
    }
    @Override
    public ServerResponse selectCourseByPage(Integer index) {
        Page<CourseEntity> page = new Page<>(index, PAGE_SIZE);
        QueryWrapper<CourseEntity> courseEntityQueryWrapper = new QueryWrapper<CourseEntity>().orderByAsc("course_order");
        IPage<CourseEntity> coursePage = course.selectPage(page, courseEntityQueryWrapper);
        return ServerResponse.ofSuccess(coursePage.getRecords());
    }

    @Override
    public int count() {
        return course.count()/PAGE_SIZE+1;
    }

    @Override
    public ServerResponse update(CourseEntity entity) {
        CourseEntity origin = course.selectById(entity.getCourseId());
        if (origin == null) {
            return ServerResponse.ofError("学生Id: " + entity.getCourseId() + "不存在!");
        }
        course.updateById(entity);
        return ServerResponse.ofSuccess("更新成功");
    }


    @Override
    public ServerResponse listName() {
        return (ServerResponse) course.selectList(null);
    }



}
