package cn.shen.sheng.da.service.impl;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.mapper.HolidayVoMapper;
import cn.shen.sheng.da.model.entity.HolidayVo;
import cn.shen.sheng.da.service.HolidayVoService;
import cn.shen.sheng.da.util.HolidayUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;

@Service
public class HolidayVoServiceImpl extends ServiceImpl<HolidayVoMapper, HolidayVo> implements HolidayVoService {
    private final HolidayVoMapper holidayVoMapper;

    @Autowired
    public HolidayVoServiceImpl(HolidayVoMapper holidayVoMapper) {
        this.holidayVoMapper = holidayVoMapper;
    }

    @Override
    public int countByYear(String year) {
        return holidayVoMapper.countByYear(Integer.parseInt(year));
    }

    @Override
    public ArrayList<HolidayVo> save(ArrayList<HolidayVo> allHolidayByYear, int year) {
        try {
            return HolidayUtil.getAllHolidayByYear(year);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void truncateTableAndResetId() {
        // 清空表数据
        holidayVoMapper.truncateTable();

        // 重置序号（适用于 MySQL 数据库）
        holidayVoMapper.resetAutoIncrement();
    }

    @Override
    public ServerResponse update(HolidayVo entity) {
        HolidayVo origin = holidayVoMapper.selectById(entity.getId());
        if (origin == null) {
            return ServerResponse.ofError("学生Id: " + entity.getId() + "不存在!");
        }

        holidayVoMapper.updateById(entity);
        return ServerResponse.ofSuccess("更新成功");
    }


}
