/*
package cn.shen.sheng.da.service.impl;

import cn.shen.sheng.da.mapper.ClassMapper;
import cn.shen.sheng.da.mapper.CourseMapper;
import cn.shen.sheng.da.mapper.ScheduleMapper;
import cn.shen.sheng.da.mapper.TeacherMapper;
import cn.shen.sheng.da.model.entity.ClassEntity;
import cn.shen.sheng.da.model.entity.CourseEntity;
import cn.shen.sheng.da.model.entity.TeacherEntity;
import cn.shen.sheng.da.model.vo.request.
import cn.shen.sheng.da.model.vo.request.CourseGene;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.genetics.*;
import org.jgap.Gene;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ScheduleInfo {

    @Autowired
    private CourseMapper courseRepository;
    @Autowired
    private ClassMapper classroomRepository;
    @Autowired
    private TeacherMapper teacherRepository;
    @Autowired
    private ScheduleMapper scheduleEntryRepository;

    public void generateSchedule(String term, String startTime) {
        List<CourseEntity> courses = courseRepository.allCourse(term);
        List<String> timeSlots = initializeTimeSlots();
        List<ClassEntity> classrooms = classroomRepository.allClasses();
        List<TeacherEntity> teachers = teacherRepository.allTeacher();

        int populationSize = 50;
        int generations = 100;
        double crossoverRate = 0.8;
        double mutationRate = 0.1;

        CrossoverPolicy crossoverPolicy = new MyCrossoverPolicy();
        MutationPolicy mutationPolicy = new MyMutationPolicy();
        SelectionPolicy selectionPolicy = new MySelectionPolicy();

        GeneticAlgorithm ga = new GeneticAlgorithm(crossoverPolicy, crossoverRate, mutationPolicy, mutationRate, selectionPolicy);

        // 3. 初始化种群：基因编码生成初始种群
        List<Chromosome> initialPopulation = generateInitialPopulation(courses.size(), classrooms.size(), timeSlots.size(), ga.getRandomGenerator().nextInt());
        Population population = new ElitisticListPopulation(initialPopulation, populationSize, 0.2);

        // 4. 执行遗传算法
        Population evolvedPopulation = ga.evolve(population, new StoppingCondition() {
            private int currentGeneration = 0;
            @Override
            public boolean isSatisfied(Population population) {
                currentGeneration++;
                return currentGeneration >= generations; // 在这里设置迭代次数的停止条件
            }
        });

        // 5. 获取最佳个体
        Chromosome bestChromosome = evolvedPopulation.getFittestChromosome();
        // 这里可以将 bestChromosome 转化为您的问题特定的解（individual），然后保存到数据库或进行其他操作
    }



    private List<Chromosome> generateInitialPopulation(int numCourses, int numClassrooms, int numTimeSlots, int populationSize) {
        List<Chromosome> initialPopulation = new ArrayList<>();

        // 循环创建初始种群中的每个染色体
        for (int i = 0; i < populationSize; i++) {
            // 创建一个染色体，并将其添加到初始种群列表
            Chromosome chromosome = createChromosome(numCourses, numClassrooms, numTimeSlots);
            initialPopulation.add(chromosome);
        }

        return initialPopulation;
    }

    private Chromosome createChromosome(int numCourses, int numClassrooms, int numTimeSlots) {
        List<Gene> genes = new ArrayList<>();

        // 循环为染色体中的每个基因创建基因对象
        for (int i = 0; i < numCourses; i++) {
            Gene gene = createGene(numClassrooms, numTimeSlots);
            genes.add(gene);
        }

        // 创建一个染色体，将基因列表传递给构造函数
        Chromosome chromosome = new CourseChromosome(genes);
        return chromosome;
    }

    private Gene createGene(int numClassrooms, int numTimeSlots) {
        // 在基因中随机选择一个教室和一个时间槽
        int randomClassroomIndex = getRandomNumber(0, numClassrooms - 1);
        int randomTimeSlotIndex = getRandomNumber(0, numTimeSlots - 1);

        // 创建一个基因对象
        Gene gene = new CourseGene(randomClassroomIndex, randomTimeSlotIndex);
        return gene;
    }

// getRandomNumber 和其他方法...


    private int getRandomNumber(int min, int max) {
        // 返回在[min, max]范围内的随机整数
        return min + (int) (Math.random() * (max - min + 1));
    }

    private double calculateFitness(String individual, List<CourseEntity> courses, List<String> timeSlots,
                                    List<ClassEntity> classrooms, List<TeacherEntity> teachers) {
        // 实现计算适应度的逻辑
        // ...
        return 0.0;
    }

    private void saveBestSchedule(List<String> bestIndividual, List<CourseEntity> courses, List<String> timeSlots,
                                  List<ClassEntity> classrooms, List<TeacherEntity> teachers) {
        // 实现将排课结果保存到数据库的逻辑
        // ...
    }

    private static List<String> initializeTimeSlots() {
        // 初始化一天中的时间段，每天两个时间段
        List<String> timeSlots = new ArrayList<>();
        timeSlots.add("Slot 1");
        timeSlots.add("Slot 2");
        return timeSlots;
    }

    private class MyCrossoverPolicy implements CrossoverPolicy {
        @Override
        public ChromosomePair crossover(Chromosome chromosome, Chromosome chromosome1) throws MathIllegalArgumentException {
            // 实现交叉逻辑...
            return null;
        }
    }

    private class MyMutationPolicy implements MutationPolicy {
        @Override
        public Chromosome mutate(Chromosome original) throws MathIllegalArgumentException {
            // 实现变异逻辑...
            return null;
        }
    }

    private class MySelectionPolicy implements SelectionPolicy {
        @Override
        public ChromosomePair select(Population population) throws MathIllegalArgumentException {
            // 实现选择逻辑...
            return null;
        }
    }
}
*/
