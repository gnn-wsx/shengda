package cn.shen.sheng.da.service.impl;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.mapper.ScheduleMapper;
import cn.shen.sheng.da.model.entity.ScheduleEntity;
import cn.shen.sheng.da.model.vo.response.TermScheduleVO;
import cn.shen.sheng.da.service.ScheduleService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ScheduleServiceImpl extends ServiceImpl<ScheduleMapper, ScheduleEntity> implements ScheduleService {
    public static final int PAGE_SIZE = 20;
    @Autowired
    private ScheduleMapper scheduleDAO;

    @Override
    public ServerResponse selectScheduleByTerm(Integer index, String term) {
        Page<TermScheduleVO> page = new Page<>(index, PAGE_SIZE);

        List<TermScheduleVO> termSchedulePage = scheduleDAO.selectScheduleByTerm(page, term);


        return ServerResponse.ofSuccess(termSchedulePage); // Assuming ServerResponse.success() is a method to create a success response.
    }

    @Override
    public List<ScheduleEntity> getClassInfoByPage(Integer index) {
        Page<ScheduleEntity> page = new Page<>(index, PAGE_SIZE);
        return scheduleDAO.selectPage(page, null).getRecords();
    }
}
