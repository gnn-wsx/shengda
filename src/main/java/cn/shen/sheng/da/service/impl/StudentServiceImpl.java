package cn.shen.sheng.da.service.impl;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.mapper.StudentMapper;
import cn.shen.sheng.da.model.entity.StudentEntity;
import cn.shen.sheng.da.model.vo.response.StudentInfoVO;
import cn.shen.sheng.da.service.StudentService;
import cn.shen.sheng.da.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, StudentEntity> implements StudentService {
    @Autowired
    private final StudentMapper mapper;
    private final UserService userService;
    public static final int PAGE_SIZE = 10;

    public StudentServiceImpl(StudentMapper mapper, UserService userService) {
        this.mapper = mapper;
        this.userService = userService;
    }

    @Override
    public List<StudentInfoVO> studentSelectLikeByName(String keyword) {
        QueryWrapper<StudentInfoVO> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("student_name", keyword);
        Page<StudentInfoVO> page = new Page<>(1, PAGE_SIZE);
        List<StudentInfoVO> classList = mapper.selectStudentInfo(page);

        return classList;
    }
    public List<StudentInfoVO> studentSelectLikeByNumber(String keyword) {
        QueryWrapper<StudentInfoVO> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("student_number", keyword);
        Page<StudentInfoVO> page = new Page<>(1, PAGE_SIZE);
        List<StudentInfoVO> classList = mapper.selectStudentInfo(page);

        return classList;
    }

    @Override
    public StudentEntity studentLogin(String number, String password) {
        QueryWrapper<StudentEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("student_number", number);
        wrapper.eq("student_password", password);
        return mapper.selectOne(wrapper);
    }

    @Override
    public StudentEntity getByNumber(String name) {

        LambdaQueryWrapper<StudentEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StudentEntity::getStudentName, name);

        return mapper.selectOne(wrapper);
    }

    @Override
    public List<StudentEntity> getClassInfoByPage(Integer index) {
        Page<StudentEntity> page = new Page<>(index, PAGE_SIZE);
        return mapper.selectPage(page, null).getRecords();
    }

    @Override
    public int count() {
        return mapper.count()/PAGE_SIZE+1;
    }

    @Override
    public ServerResponse update(StudentEntity entity) {
        StudentEntity origin = mapper.selectById(entity.getStudentId());
        if (origin == null) {
            return ServerResponse.ofError("学生Id: " + entity.getStudentId() + "不存在!");
        }
        if (entity.getPassword().equals("")) {
            entity.setPassword(origin.getPassword());
        } else {
            entity.setPassword(userService.computePasswordHash(entity.getPassword()));
        }

        mapper.updateById(entity);
        return ServerResponse.ofSuccess("更新成功");
    }


    @Override
    public ServerResponse allStudentInfo(Integer index) {
        Page<StudentInfoVO> page = new Page<>(index, PAGE_SIZE);
        return (ServerResponse) mapper.selectStudentInfo(page);
    }
}
