package cn.shen.sheng.da.service.impl;

import cn.shen.sheng.da.common.ServerResponse;
import cn.shen.sheng.da.mapper.TeacherMapper;
import cn.shen.sheng.da.model.entity.TeacherEntity;
import cn.shen.sheng.da.model.vo.response.TeacherInfoVO;
import cn.shen.sheng.da.service.TeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, TeacherEntity> implements TeacherService {
    @Autowired
    private final TeacherMapper mapper;
    public static final int PAGE_SIZE = 10;

    public TeacherServiceImpl(TeacherMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public ServerResponse teacherInfoList(Integer index) {
        QueryWrapper<TeacherInfoVO> queryWrapper = new QueryWrapper<TeacherInfoVO>().orderByAsc("teacher_id");
        Page<TeacherInfoVO> page = new Page<>(index, PAGE_SIZE);
        List<TeacherInfoVO> teacherInfoVOList = mapper.selectTeacherInfo(page,queryWrapper).getRecords();
        return ServerResponse.ofSuccess(teacherInfoVOList);
    }

    @Override
    public ServerResponse teacherInfoLikeByName(String keyword) {
        QueryWrapper<TeacherInfoVO> queryWrapper = new QueryWrapper<TeacherInfoVO>();
        queryWrapper.like("teacher_name", keyword);
        Page<TeacherInfoVO> page = new Page<>(1, PAGE_SIZE);
        IPage<TeacherInfoVO> teacherInfoVOListByName = mapper.selectTeacherInfo(page,queryWrapper);
        ServerResponse response = new ServerResponse();
        response.setData(teacherInfoVOListByName.getRecords());

        return ServerResponse.ofSuccess(response);

    }

    @Override
    public ServerResponse teacherInfoLikeByNumber(String keyword) {
        QueryWrapper<TeacherInfoVO> queryWrapper = new QueryWrapper<TeacherInfoVO>();
        queryWrapper.like("teacher_number", keyword);
        Page<TeacherInfoVO> page = new Page<>(1, PAGE_SIZE);
        IPage<TeacherInfoVO> teacherInfoVOListByName = mapper.selectTeacherInfo(page,queryWrapper);
        ServerResponse response = new ServerResponse();
        response.setData(teacherInfoVOListByName.getRecords());

        return ServerResponse.ofSuccess(response);

    }

    @Override
    public ServerResponse getClassInfoByPage(Integer index) {
        Page<TeacherEntity> page = new Page<>(index, PAGE_SIZE);
        return ServerResponse.ofSuccess(mapper.selectPage(page, null).getRecords()) ;
    }

    @Override
    public TeacherEntity teacherLogin(String number, String password) {
        QueryWrapper<TeacherEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("teacher_number", number);
        wrapper.eq("teacher_password", password);
        return mapper.selectOne(wrapper);
    }

    @Override
    public int count() {
        return mapper.count()/PAGE_SIZE+1;
    }
}
