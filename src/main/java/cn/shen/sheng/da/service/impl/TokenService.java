package cn.shen.sheng.da.service.impl;

import cn.shen.sheng.da.model.entity.AdminEntity;
import cn.shen.sheng.da.model.entity.StudentEntity;
import cn.shen.sheng.da.model.entity.TeacherEntity;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author: 15760
 * @Date: 2020/3/24
 * @Descripe: Token下发
 */

@Service
public class TokenService {

    /**
     * 验证学生
     * @param student
     * @return
     */
    public String getToken(StudentEntity student) {
        Date start = new Date();
        // 一小时有效时间
        long currentTime = System.currentTimeMillis() + 60* 60 * 500;
        Date end = new Date(currentTime);
        String token = "";

        token = JWT.create().withAudience(student.getStudentId().toString()).withIssuedAt(start).withExpiresAt(end)
                .sign(Algorithm.HMAC256(student.getPassword()));
        return token;
    }

    /**
     * 验证管理员
     * @param admin
     * @return
     */
    public String getToken(AdminEntity admin) {
        Date start = new Date();
        long currentTime = System.currentTimeMillis() + 60* 60 * 500;
        Date end = new Date(currentTime);
        String token = "";

        token = JWT.create().withAudience(admin.getId().toString()).withIssuedAt(start).withExpiresAt(end)
                .sign(Algorithm.HMAC256(admin.getPassword()));
        return token;
    }

    /**
     * 验证讲师
     * @param teacher
     * @return
     */
    public String getToken(TeacherEntity teacher) {
        Date start = new Date();
        long currentTime = System.currentTimeMillis() + 60* 60 * 500;
        Date end = new Date(currentTime);
        String token = "";

        token = JWT.create().withAudience(teacher.getTeacherId().toString()).withIssuedAt(start).withExpiresAt(end)
                .sign(Algorithm.HMAC256(teacher.getPassword()));
        return token;
    }

}
