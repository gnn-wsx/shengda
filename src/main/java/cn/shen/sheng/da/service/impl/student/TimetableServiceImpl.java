package cn.shen.sheng.da.service.impl.student;

import cn.shen.sheng.da.manager.StudentLoginStatusDao;
import cn.shen.sheng.da.mapper.StudentMapper;
import cn.shen.sheng.da.model.vo.response.StudentScheduleVO;
import cn.shen.sheng.da.service.student.TimetableServiceS;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("student_timetableService")
public class TimetableServiceImpl  implements TimetableServiceS {


    public final StudentMapper mapper;
    private final StudentLoginStatusDao statusDao;

    public TimetableServiceImpl(StudentMapper mapper, StudentLoginStatusDao statusDao) {
        this.mapper = mapper;

        this.statusDao = statusDao;
    }

    @Override
    public List<StudentScheduleVO> get() {
        Integer studentId = statusDao.getUserId();
        Integer classId = mapper.selectClass(studentId).getClassId();
        return mapper.listStudentTimetable(classId);
    }
}
