package cn.shen.sheng.da.service.impl.teacher;

import cn.shen.sheng.da.manager.StudentLoginStatusDao;
import cn.shen.sheng.da.mapper.TeacherMapper;
import cn.shen.sheng.da.model.vo.response.TeacherScheduleVO;
import cn.shen.sheng.da.service.teacher.TimetableServiceT;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("teacher_timetableService")
public class TimetableServiceImpl implements TimetableServiceT {


    public final TeacherMapper mapper;
    private final StudentLoginStatusDao statusDao;

    public TimetableServiceImpl(TeacherMapper mapper, StudentLoginStatusDao statusDao) {
        this.mapper = mapper;
        this.statusDao = statusDao;
    }


    @Override
    public List<TeacherScheduleVO> get() {
        Integer teacherId = statusDao.getUserId();
        return mapper.listStudentTimetable(teacherId);
    }
}
