package cn.shen.sheng.da.service.student;

import cn.shen.sheng.da.model.vo.response.StudentScheduleVO;

import java.util.List;


public interface TimetableServiceS {
    List<StudentScheduleVO> get();
}
