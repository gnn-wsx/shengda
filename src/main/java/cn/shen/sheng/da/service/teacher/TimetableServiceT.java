package cn.shen.sheng.da.service.teacher;

import cn.shen.sheng.da.model.vo.response.TeacherScheduleVO;

import java.util.List;


public interface TimetableServiceT {
    List<TeacherScheduleVO> get();
}
