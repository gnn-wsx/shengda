package cn.shen.sheng.da.util;

import cn.shen.sheng.da.model.entity.HolidayVo;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class TeatDate {
    public static void main(String[] args) {
        int year = getCurrentYear();
        try {
            ArrayList<HolidayVo> holidayVoList = HolidayUtil.getAllHolidayByYear(year);
            System.out.println("全年完整数据：");
            for (HolidayVo holidayVo : holidayVoList) {
                System.out.println(holidayVo);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int getCurrentYear() {
        LocalDate currentDate = LocalDate.now();
        return currentDate.getYear();
    }
}

