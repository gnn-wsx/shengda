import cn.shen.sheng.da.CoursearrangeApplication;
import cn.shen.sheng.da.mapper.ClassMapper;
import cn.shen.sheng.da.model.vo.response.ClassInfoVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CoursearrangeApplication.class)
public class ClassInfoMapperTest {

    @Autowired
    private ClassMapper classInfoMapper;

    @Test
    public void testClassInfoQuery() {
        List<ClassInfoVO> classInfoList = classInfoMapper.classInfo2();
        System.out.println("classInfoList = " + classInfoList);

}
}
